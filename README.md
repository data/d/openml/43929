# OpenML dataset: Meta-Album--BCT--(Micro)

https://www.openml.org/d/43929

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Digital Image of Bacterial Species (DIBaS)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43929) of an [OpenML dataset](https://www.openml.org/d/43929). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43929/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43929/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43929/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

